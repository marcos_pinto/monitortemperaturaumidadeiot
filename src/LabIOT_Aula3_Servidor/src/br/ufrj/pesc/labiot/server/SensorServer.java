package br.ufrj.pesc.labiot.server;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.RaspiPin;

import br.ufrj.pesc.labiot.server.hardware.LedAdapter;
import br.ufrj.pesc.labiot.server.hardware.SensorAdapter;
import br.ufrj.pesc.labiot.server.resources.*;

/**
 * Classe respons�vel pelo controle do servidor COAP e monitoramento do sensor.
 * O monitoramento � feito com o uso de um timer, que acessa o sensor e armazena
 * os valores atuais em uma classe de ambiente EnvironmentSingleton, visivel
 * pelo projeto e que se comporta como um cache. Ao receber uma requisi��o do
 * cliente o servidor devolve os valores armazenados na EnvironmentSingleton.
 *
 */
public class SensorServer extends CoapServer {

	private static int temperaturePort = 29;
	private static int humidityPort = 28;
	private static GpioController controller = GpioFactory.getInstance();
	private static LedAdapter temperatureLedAdapter = new LedAdapter(RaspiPin.getPinByAddress(temperaturePort),controller);
	private static LedAdapter humidityLedAdapter = new LedAdapter(RaspiPin.getPinByAddress(humidityPort),controller);
	private static SensorAdapter sensorAdapter = new SensorAdapter();

	/*
	 * Metodo principal que inicia o servidor e monitora o sensor
	 */
	public static void main(String[] args) throws IOException, InterruptedException {
		System.out.println("Iniciando o servidor");

		// Inicialmente troca o status dos LEDs para desligado
		//ledAdapter.changeState(RaspiPin.getPinByAddress(temperaturePort), true, true);
		Thread threadT = new Thread(temperatureLedAdapter, "Thread Temperature");
		threadT.start();
		
		Thread threadH = new Thread(humidityLedAdapter, "Thread Humidity");
		threadH.start();

		
		//ledAdapter.changeState(RaspiPin.getPinByAddress(humidityPort), true, true);

		// Inicia os resources
		ObservableResource obsRes = startCoapServer();

		// Faz medi��o inicial
		InstantMeasure currentMeasure = sensorAdapter.readSensor();

		// Armazena a medi��o inicial
		EnvironmentSingleton.instance().setCurrentMeasure(currentMeasure);

		// Configura timer de 1s para monitorar o sensor
		Timer timer = new Timer();
		timer.schedule(new UpdateTask(obsRes), 0, 1000);
	}

	/*
	 * Inicializa��o do sevidor COAP com os resources de setup e observa��o
	 */
	private static ObservableResource startCoapServer() {
		SensorServer server = new SensorServer();

		SetupResource setupResource = new SetupResource("setup-resource");
		server.add(setupResource);

		ObservableResource obsRes = new ObservableResource("observable-resource");
		obsRes.setObservable(true);
		obsRes.getAttributes().setObservable();
		server.add(obsRes);

		server.start();

		return obsRes;
	}

	/*
	 * InnerClass respons�vel pelo monitoramento do sensor.
	 */
	private static class UpdateTask extends TimerTask {

		private CoapResource mCoapRes;

		public UpdateTask(CoapResource coapRes) {
			mCoapRes = coapRes;
		}

		@Override
		public void run() {

			System.out.println("Realizando leitura do sensor");

			// Realiza a leitura do sensor
			InstantMeasure currentMeasure = sensorAdapter.readSensor();

			System.out.println("Resultado da leitura: " + currentMeasure.toString());

			// Armazena no singleton a medi��o atual
			EnvironmentSingleton.instance().setCurrentMeasure(currentMeasure);
 
			// Se as medi��es ultrapassaram os valores limite, acende os LEDs
			boolean temperatureAbove = EnvironmentSingleton.instance().isTemperatureAboveThreshold();
			boolean humidityAbove = EnvironmentSingleton.instance().isHumidityAboveThreshold();
			boolean blink=false;
			
			if(temperatureAbove && humidityAbove)
				blink=true;
			
			temperatureLedAdapter.setBlink(blink);
			humidityLedAdapter.setBlink(blink);
						
			temperatureLedAdapter.setState(temperatureAbove);
			humidityLedAdapter.setState(humidityAbove);		

			// Notifica os clientes
			mCoapRes.changed();

		}
	}

}