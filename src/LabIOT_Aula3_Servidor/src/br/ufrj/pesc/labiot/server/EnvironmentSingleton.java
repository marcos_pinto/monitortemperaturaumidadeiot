package br.ufrj.pesc.labiot.server;

/*
 * Classe que representa um singleton que armazena os dados da ultima leitura do sensor e valores limite
 * */
public final class EnvironmentSingleton {

	private static final EnvironmentSingleton singletonObj = new EnvironmentSingleton();

	public static EnvironmentSingleton instance() {
		return singletonObj;
	}

	private EnvironmentSingleton() {
		super();
		temperatureThreshold = 25;
		humidityThreshold = 88;
		currentMeasure = new InstantMeasure(0, 0);
	}

	/* Obtem a temperatura limite */
	public int getTemperatureThreshold() {
		return temperatureThreshold;
	}

	/* Atualiza a temperatura limite */
	public void setTemperatureThreshold(int temperatureThreshold) {
		this.temperatureThreshold = temperatureThreshold;
	}

	/* Obtem a umidade limite */
	public int getHumidityThreshold() {
		return humidityThreshold;
	}

	/* Atualiza a umidade limite */
	public void setHumidityThreshold(int humidityThreshold) {
		this.humidityThreshold = humidityThreshold;
	}

	/* Obtem a ultima medi��o realizada */
	public InstantMeasure getCurrentMeasure() {
		return currentMeasure;
	}

	/* Atualiza a medi��o atual */
	public void setCurrentMeasure(InstantMeasure currentMeasure) {
		this.currentMeasure = currentMeasure;
		temperatureLEDOn = isTemperatureAboveThreshold();
		humidityLEDOn = isHumidityAboveThreshold();
	}

	private int temperatureThreshold;
	private int humidityThreshold;
	private InstantMeasure currentMeasure;
	private boolean temperatureLEDOn;
	private boolean humidityLEDOn;

//	/*Retorna se a ultima temperatura registrada est� acima do limite*/
	public boolean isTemperatureAboveThreshold() {
		return currentMeasure.getTemperature() > temperatureThreshold;
	}

	/* Retorna se a ultima umidade registrada est� acima do limite */
	public boolean isHumidityAboveThreshold() {
		return currentMeasure.getHumidity() > humidityThreshold;
	}

}
