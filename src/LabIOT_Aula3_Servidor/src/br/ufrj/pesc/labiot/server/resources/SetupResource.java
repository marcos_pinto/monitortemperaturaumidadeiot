package br.ufrj.pesc.labiot.server.resources;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.server.resources.CoapExchange;

import br.ufrj.pesc.labiot.server.EnvironmentSingleton;

/* Resource que atende a resquisições de troca dos valores limite de temperatura e umidade*/
public class SetupResource extends CoapResource {

	public SetupResource(String name) {
		super(name);
	}

	/* Metodo responsável por atender as requisições POST de troca de limites */
	@Override
	public void handlePOST(CoapExchange exchange) {
		String text = exchange.getRequestText();
		String[] tokens = text.split(";");

		EnvironmentSingleton.instance().setTemperatureThreshold(Integer.parseInt(tokens[0]));
		EnvironmentSingleton.instance().setHumidityThreshold(Integer.parseInt(tokens[1]));

		exchange.respond(ResponseCode.CONTENT, "Novos Limites configurados", MediaTypeRegistry.APPLICATION_JSON);

	}

}