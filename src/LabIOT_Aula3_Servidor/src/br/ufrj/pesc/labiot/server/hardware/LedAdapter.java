package br.ufrj.pesc.labiot.server.hardware;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

/*
 * Classe respons�vel por abstrair o acesso os LEDs do projeto
 * */
public class LedAdapter implements Runnable  {
	private boolean blink;
	private Pin p;
	private boolean on;
	GpioController controller; 	
	
	public LedAdapter(Pin p, GpioController controller) 
	{
		this.p = p;
		on=false;
		blink=false;
		this.controller = controller; 
	}	

	/*
	 * Troca o status de um LED configurado em um pino definido no construtor da classe
	 * O uso de threading foi motivado pelo fato do metodo pin.blink n�o ter funcionado,
	 * logo para fazer o LED piscar em um loop (usando toggle) n�o podemos fazer um loop
	 * em um objeto que n�o est� em Thread, visto que o mesmo travaria a Thread principal,
	 * fazendo com que o servidor ficasse parado.
	 * Essa classe mantem o estado dos LEDS (ligado, ou piscando), e no loop verifica esse
	 * estado para acionar o hardware
	 */
	@Override
	public void run() {
		System.out.println("Trocando o estado do LED " + p.getAddress() + " para " + on);		
		
		final GpioPinDigitalOutput pin = controller.provisionDigitalOutputPin(p);
		
		pin.setShutdownOptions(true, PinState.LOW);

		while (true) {
			if ((on) && (!blink))
				pin.high();
			else if (blink)
			{
				pin.toggle();					
			}			
			else
				pin.low();
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e)
			{				
				e.printStackTrace();
			}
			//System.out.println("Estado do LED :"+pin.getName()+" "+on);
		}
	}

	public void setState(boolean temperatureAbove) {
		this.on=temperatureAbove;		
	}

	public void setBlink(boolean blink) {
		this.blink=blink;	
	}	
}
