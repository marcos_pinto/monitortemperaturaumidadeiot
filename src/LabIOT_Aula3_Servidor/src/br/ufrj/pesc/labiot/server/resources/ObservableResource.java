package br.ufrj.pesc.labiot.server.resources;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.server.resources.CoapExchange;

import com.google.gson.Gson;

import br.ufrj.pesc.labiot.server.EnvironmentSingleton;

/* Resource que representa um observable que retorna a medi��o dos valores do sensor 
 * e valores limite atualmente configurados*/
public class ObservableResource extends CoapResource {

	public ObservableResource(String name) {
		super(name);
	}

	/*
	 * Retorna a medi��o atual no formato JSON aos clientes que invocaram o verbo
	 * GET do servidor
	 */
	@Override
	public void handleGET(CoapExchange exchange) {

		Gson gson = new Gson();
		String jsonString = gson.toJson(EnvironmentSingleton.instance());
		System.out.println("Enviando json: " + jsonString);
		exchange.respond(ResponseCode.CONTENT, jsonString, MediaTypeRegistry.TEXT_PLAIN);
	}
}