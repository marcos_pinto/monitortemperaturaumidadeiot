package br.ufrj.pesq.labiot.client;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapObserveRelation;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.Utils;
import org.eclipse.californium.core.coap.Request;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.eclipse.californium.core.coap.CoAP.Code;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JTextField;

import javax.swing.JLabel;

/**
 * Cliente que estabelece conex�o com o servidor instalado no Raspberry
 *
 */
public class ClientForm extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	static ClientForm theFrameIOTPart;

	JPanel pnPanel0;
	JButton btBtnUpdate;
	ButtonGroup rbgPanel0;
	JTextField tfTxtTemperatureThreshold;
	JLabel lbLbTemperatureThreshold;
	JTextField txtHumidityThreshold;
	JLabel lbLblHumidityThreshold;
	JTextField tfTxtAddress;
	JLabel lbLblEndereco;
	JButton btBtnConnect;
	JLabel lbLblTemperatureLabel;
	JLabel lbLblActualTemperature;
	JLabel lbLblHumidityLabel;
	JLabel lbLblActualHumidity;
	JButton tbtLblTemperatureLED;
	JButton tbtLblHumidityLED;
	JTextArea taTxtLog;

	// private static String _address = "coap://192.168.0.108:5683";
	private static String _address = "coap://10.0.0.249:5683";
	private static String _initTemp = "25";
	private static String _initHum = "88";

	/**
	 */
	public static void main(String args[]) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		} catch (UnsupportedLookAndFeelException e) {
		}
		theFrameIOTPart = new ClientForm();
	}

	/**
	 */
	public ClientForm() {
		super("Projeto IOT - Aula 3");

		pnPanel0 = new JPanel();
		pnPanel0.setSize(500, 600);
		pnPanel0.setBorder(BorderFactory.createTitledBorder("Monitor de Temperatura e Umidade"));
		rbgPanel0 = new ButtonGroup();

		GridBagLayout gbPanel0 = new GridBagLayout();
		GridBagConstraints gbcPanel0 = new GridBagConstraints();
		pnPanel0.setLayout(gbPanel0);

		btBtnUpdate = new JButton("Atualizar Limites");
		gbcPanel0.gridx = 9;
		gbcPanel0.gridy = 6;
		gbcPanel0.gridwidth = 1;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 1;
		gbcPanel0.weighty = 0;
		gbcPanel0.anchor = GridBagConstraints.NORTHWEST;
		gbcPanel0.insets = new Insets(0, 0, 50, 0);
		gbPanel0.setConstraints(btBtnUpdate, gbcPanel0);
		pnPanel0.add(btBtnUpdate);

		tfTxtTemperatureThreshold = new JTextField();
		tfTxtTemperatureThreshold.setText(_initTemp);
		tfTxtTemperatureThreshold.setColumns(15);
		gbcPanel0.gridx = 4;
		gbcPanel0.gridy = 6;
		gbcPanel0.gridwidth = 1;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 1;
		gbcPanel0.weighty = 1;
		gbcPanel0.anchor = GridBagConstraints.NORTH;
		gbcPanel0.insets = new Insets(0, 0, 50, 4);
		gbPanel0.setConstraints(tfTxtTemperatureThreshold, gbcPanel0);
		pnPanel0.add(tfTxtTemperatureThreshold);

		lbLbTemperatureThreshold = new JLabel("Novo limite de temperatura");
		gbcPanel0.gridx = 4;
		gbcPanel0.gridy = 5;
		gbcPanel0.gridwidth = 1;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 1;
		gbcPanel0.weighty = 1;
		gbcPanel0.anchor = GridBagConstraints.SOUTH;
		gbcPanel0.insets = new Insets(0, 0, 0, 0);
		gbPanel0.setConstraints(lbLbTemperatureThreshold, gbcPanel0);
		pnPanel0.add(lbLbTemperatureThreshold);

		txtHumidityThreshold = new JTextField();
		txtHumidityThreshold.setText(_initHum);
		txtHumidityThreshold.setColumns(15);
		gbcPanel0.gridx = 7;
		gbcPanel0.gridy = 6;
		gbcPanel0.gridwidth = 1;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 0;
		gbcPanel0.weighty = 0;
		gbcPanel0.anchor = GridBagConstraints.NORTH;
		gbcPanel0.insets = new Insets(0, 0, 50, 0);
		gbPanel0.setConstraints(txtHumidityThreshold, gbcPanel0);
		pnPanel0.add(txtHumidityThreshold);

		lbLblHumidityThreshold = new JLabel("Novo limite de umidade");

		gbcPanel0.gridx = 7;
		gbcPanel0.gridy = 5;
		gbcPanel0.gridwidth = 1;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 1;
		gbcPanel0.weighty = 1;
		gbcPanel0.anchor = GridBagConstraints.SOUTH;
		gbcPanel0.insets = new Insets(0, 0, 0, 15);
		gbPanel0.setConstraints(lbLblHumidityThreshold, gbcPanel0);
		pnPanel0.add(lbLblHumidityThreshold);

		tfTxtAddress = new JTextField();
		tfTxtAddress.setText(_address);
		tfTxtAddress.setColumns(40);
		gbcPanel0.gridx = 4;
		gbcPanel0.gridy = 1;
		gbcPanel0.gridwidth = 4;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 1;
		gbcPanel0.weighty = 1;
		gbcPanel0.anchor = GridBagConstraints.NORTH;
		gbPanel0.setConstraints(tfTxtAddress, gbcPanel0);
		pnPanel0.add(tfTxtAddress);

		lbLblEndereco = new JLabel("Endere�o do servidor");
		gbcPanel0.gridx = 4;
		gbcPanel0.gridy = 0;
		gbcPanel0.gridwidth = 1;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 1;
		gbcPanel0.weighty = 1;
		gbcPanel0.anchor = GridBagConstraints.SOUTH;
		gbcPanel0.insets = new Insets(0, 0, 0, 18);
		gbPanel0.setConstraints(lbLblEndereco, gbcPanel0);
		pnPanel0.add(lbLblEndereco);

		btBtnConnect = new JButton("Conectar");
		gbcPanel0.gridx = 9;
		gbcPanel0.gridy = 1;
		gbcPanel0.gridwidth = 1;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 1;
		gbcPanel0.weighty = 0;
		gbcPanel0.anchor = GridBagConstraints.NORTHWEST;
		gbPanel0.setConstraints(btBtnConnect, gbcPanel0);
		pnPanel0.add(btBtnConnect);

		lbLblTemperatureLabel = new JLabel("Temperatura atual");
		gbcPanel0.gridx = 4;
		gbcPanel0.gridy = 10;
		gbcPanel0.gridwidth = 1;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 0;
		gbcPanel0.weighty = 1;
		gbcPanel0.anchor = GridBagConstraints.CENTER;
		gbcPanel0.insets = new Insets(0, 0, 10, 0);
		gbPanel0.setConstraints(lbLblTemperatureLabel, gbcPanel0);
		pnPanel0.add(lbLblTemperatureLabel);

		lbLblActualTemperature = new JLabel("-");
		gbcPanel0.gridx = 7;
		gbcPanel0.gridy = 10;
		gbcPanel0.gridwidth = 1;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 0;
		gbcPanel0.weighty = 1;
		gbcPanel0.anchor = GridBagConstraints.CENTER;
		gbcPanel0.insets = new Insets(0, 0, 10, 0);
		gbPanel0.setConstraints(lbLblActualTemperature, gbcPanel0);
		pnPanel0.add(lbLblActualTemperature);

		lbLblHumidityLabel = new JLabel("Umidade atual");
		gbcPanel0.gridx = 4;
		gbcPanel0.gridy = 12;
		gbcPanel0.gridwidth = 1;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 0;
		gbcPanel0.weighty = 1;
		gbcPanel0.anchor = GridBagConstraints.NORTH;
		gbcPanel0.insets = new Insets(0, 0, 0, 10);
		gbPanel0.setConstraints(lbLblHumidityLabel, gbcPanel0);
		pnPanel0.add(lbLblHumidityLabel);

		lbLblActualHumidity = new JLabel("-");
		gbcPanel0.gridx = 7;
		gbcPanel0.gridy = 12;
		gbcPanel0.gridwidth = 1;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 0;
		gbcPanel0.weighty = 1;
		gbcPanel0.anchor = GridBagConstraints.NORTH;
		gbPanel0.setConstraints(lbLblActualHumidity, gbcPanel0);
		pnPanel0.add(lbLblActualHumidity);

		tbtLblTemperatureLED = new JButton("");
		tbtLblTemperatureLED.setEnabled(false);
		tbtLblTemperatureLED.setContentAreaFilled(false);
		tbtLblTemperatureLED.setOpaque(true);
		
		rbgPanel0.add(tbtLblTemperatureLED);
		gbcPanel0.gridx = 8;
		gbcPanel0.gridy = 10;
		gbcPanel0.gridwidth = 1;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 0;
		gbcPanel0.weighty = 0;
		gbcPanel0.anchor = GridBagConstraints.WEST;
		gbPanel0.setConstraints(tbtLblTemperatureLED, gbcPanel0);
		pnPanel0.add(tbtLblTemperatureLED);

		tbtLblHumidityLED = new JButton("");
		tbtLblHumidityLED.setEnabled(false);	
		tbtLblHumidityLED.setContentAreaFilled(false);
		tbtLblHumidityLED.setOpaque(true);

		

		rbgPanel0.add(tbtLblHumidityLED);
		gbcPanel0.gridx = 8;
		gbcPanel0.gridy = 12;
		gbcPanel0.gridwidth = 1;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.NONE;
		gbcPanel0.weightx = 0;
		gbcPanel0.weighty = 0;
		gbcPanel0.anchor = GridBagConstraints.NORTHWEST;
		gbPanel0.setConstraints(tbtLblHumidityLED, gbcPanel0);
		pnPanel0.add(tbtLblHumidityLED);

		taTxtLog = new JTextArea(2, 10);
		JScrollPane sp = new JScrollPane(taTxtLog);

		gbcPanel0.gridx = 4;
		gbcPanel0.gridy = 14;
		gbcPanel0.gridwidth = 10;
		gbcPanel0.gridheight = 1;
		gbcPanel0.fill = GridBagConstraints.BOTH;
		gbcPanel0.weightx = 1;
		gbcPanel0.weighty = 1;
		gbcPanel0.anchor = GridBagConstraints.SOUTH;
		gbcPanel0.insets = new Insets(0, 30, 0, 30);
		gbPanel0.setConstraints(sp, gbcPanel0);

		pnPanel0.add(sp);

		// Evento que captura o clique no bot�o de conex�o
		btBtnConnect.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				// Cria��o do cliente COAP
				CoapClient client = new CoapClient(tfTxtAddress.getText() + "/observable-resource");

				taTxtLog.append("Iniciando conex�o\n\r");

				// Evento de retorno da mensagem do observador
				@SuppressWarnings("unused")
				CoapObserveRelation rel = client.observeAndWait(new CoapHandler() {

					@Override
					public void onLoad(CoapResponse response) {

						// Ao receber o JSON enviado pelo servidor, realiza o parse e altera os
						// componentes visuais
						JsonObject jsonObject = (JsonObject) new JsonParser()
								.parse(response.advanced().getPayloadString());
 
						String temperatureThreshold = jsonObject.get("temperatureThreshold").getAsString();
						String humidityThreshold = jsonObject.get("humidityThreshold").getAsString();
						boolean temperatureLEDOn = jsonObject.get("temperatureLEDOn").getAsBoolean();
						boolean humidityLEDOn = jsonObject.get("humidityLEDOn").getAsBoolean();

						String temperature = jsonObject.getAsJsonObject("currentMeasure").get("temperature")
								.getAsString();
						String humidity = jsonObject.getAsJsonObject("currentMeasure").get("humidity").getAsString();

						lbLblActualTemperature.setText(temperature + "  (limite = " + temperatureThreshold + ")");
						lbLblActualHumidity.setText(humidity + "  (limite = " + humidityThreshold + ")");

						Color tempColor = temperatureLEDOn ? Color.red : Color.GRAY;
						Color humiColor = humidityLEDOn ? Color.green : Color.GRAY;

						tbtLblHumidityLED.setForeground(humiColor);
						tbtLblHumidityLED.setBackground(humiColor);

						tbtLblTemperatureLED.setForeground(tempColor);
						tbtLblTemperatureLED.setBackground(tempColor);

						taTxtLog.append("Recebido: " + response.advanced().getPayloadString() + "\n\r");
						taTxtLog.setCaretPosition(taTxtLog.getDocument().getLength());
					}

					@Override
					public void onError() {
						taTxtLog.append("Erro na conex�o com o servidor");
						taTxtLog.setCaretPosition(taTxtLog.getDocument().getLength());
					}
				});
			}
		});

		// Evento que captura o clique no bot�o de troca de limites
		btBtnUpdate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				CoapClient client = new CoapClient(tfTxtAddress.getText() + "/setup-resource");
				Request initrequest = new Request(Code.POST);
				initrequest.setPayload(tfTxtTemperatureThreshold.getText() + ";" + txtHumidityThreshold.getText());

				CoapResponse coapResp = client.advanced(initrequest);
				System.out.println(Utils.prettyPrint(coapResp));
				taTxtLog.append(Utils.prettyPrint(coapResp) + "\n\r");
				taTxtLog.setCaretPosition(taTxtLog.getDocument().getLength());

			}
		});

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setContentPane(pnPanel0);
		pack();
		setSize(660, 500);
		setVisible(true);
	}
}
